let angka = 1

//sebelum menghandle asynchronous
console.log("sebelum menghandle asynchronous")

const beforeAsync = (angka) => {
    angka = 2
    console.log(angka)
}

beforeAsync(angka)
console.log(angka)
console.log()

//setelah menghandle asynchronous
console.log("setelah menghandle asynchronous")

const myFunctionPromise = (angka) => {
    return new Promise((resolve, reject) => {
        resolve(angka = 2)
    })
}

const afterAsync = async (param) => {
    let output = await myFunctionPromise(param)
    console.log(output)
}

afterAsync(angka)
console.log(angka)