function periksaAntrianDokter(nomerAntri, callback) {
    console.log(`sekarang antrian ke-${nomerAntri}`)
    setTimeout(function () {
        if (nomerAntri === 10) {
            console.log("saya masuk ruangan dokter")
            callback(0)
        } else {
            console.log("saya masih menunggu")
            callback(nomerAntri + 1)
        }
    }, 1000)
}

function execute(nomorAntri) {
    periksaAntrianDokter(nomorAntri, function (nomorAntriBaru) {
        if (nomorAntriBaru !== 0) {
            execute(nomorAntriBaru)
        }
    })
}

execute(4)