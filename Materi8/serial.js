const firstPromise = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("first promise")
        }, 1000)
    })
}

const secondPromise = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("second promise")
        }, 1000)
    })
}

const thirdPromise = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("third promise")
        }, 1000)
    })
}

//ini paralel selama satu detik
const asyncParalel = async () => {
    firstPromise().then(res => {
        console.log(res)
    })
    secondPromise().then(res => {
        console.log(res)
    })
    thirdPromise().then(res => {
        console.log(res)
    })

}

// ini berseri selama tiga detik
const asyncSerial = async () => {
    let a = await firstPromise()
    console.log(a)
    let b = await secondPromise()
    console.log(b)
    let c = await thirdPromise()
    console.log(c)
}

asyncParalel()
asyncSerial()