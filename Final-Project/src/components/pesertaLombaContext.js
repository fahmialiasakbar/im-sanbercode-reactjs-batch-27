import axios from "axios"
import React, { createContext, useState } from "react"
import { useHistory } from "react-router-dom"

export const PesertaLombaContext = createContext()

export const PesertaLombaProvider = props => {
    let history = useHistory()
    const [pesertaLomba, setPesertaLomba] = useState([])
    const [inputName, setInputName] = useState("")
    const [currentIndex, setCurrentIndex] = useState(-1)
    const [fetchStatus, setFetchStatus] = useState(false)

    const fetchData = async () => {
        let result = await axios.get(`http://backendexample.sanbercloud.com/api/contestants`)
        let data = result.data
        let output = data.map((e) => {
            return {
                id: e.id,
                name: e.name
            }
        })

        setPesertaLomba(output)
    }

    const fetchDataById = async (idPeserta) => {
        axios.get(`http://backendexample.sanbercloud.com/api/contestants/${idPeserta}`)
            .then((res) => {
                let data = res.data
                setInputName(data.name)
                setCurrentIndex(data.id)
            })
    }

    const functionDelete = (idPeserta) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/contestants/${idPeserta}`)
            .then(() => {
                let newPesertaLomba = pesertaLomba.filter((e) => { return e.id !== idPeserta })
                setPesertaLomba(newPesertaLomba)
            })
    }

    const functionSubmit = (params) => {
        axios.post(`http://backendexample.sanbercloud.com/api/contestants`, {
            name: inputName
        })
            .then((res) => {
                console.log(res)
                setPesertaLomba([...pesertaLomba, { id: res.data.id, name: res.data.name }])
                history.push('/peserta')
            })
    }

    const functionUpdate = (params) => {
        axios.put(`http://backendexample.sanbercloud.com/api/contestants/${currentIndex}`, {
            name: inputName
        })
            .then((res) => {
                let updatedItem = pesertaLomba.find((e) => e.id === currentIndex)
                updatedItem.name = inputName
                setPesertaLomba([...pesertaLomba])
                history.push('/peserta')
            })
    }

    const functionEdit = (idPeserta) => {

        history.push(`/peserta/form/${idPeserta}`)


    }

    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit,
        fetchDataById
    }

    return (
        <PesertaLombaContext.Provider value={{
            pesertaLomba,
            setPesertaLomba,
            inputName,
            setInputName,
            currentIndex,
            setCurrentIndex,
            functions,
            fetchStatus, setFetchStatus
        }}>
            {props.children}
        </PesertaLombaContext.Provider>
    )
}