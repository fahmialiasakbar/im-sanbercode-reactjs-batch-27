import React from "react"
import { PesertaLombaProvider } from "./pesertaLombaContext"
import PesertaLombaForm from "./pesertaLombaForm"
import PesertaLombaList from "./pesertaLombaList"

const PesertaLomba = () => {
    return(
        <PesertaLombaProvider>
            <PesertaLombaList/>
            <PesertaLombaForm/>
        </PesertaLombaProvider>
    )
}

export default PesertaLomba