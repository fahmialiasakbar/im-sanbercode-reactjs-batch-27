import React, { useContext, useEffect } from "react"
import { Link } from "react-router-dom"
import { Button } from "antd"
import { MovieContext } from "./movieContext"
import { LeftOutlined } from '@ant-design/icons'
import { useParams } from "react-router-dom"

const MovieDetail = () => {
    let { id } = useParams()

    const { movie, functions, detailMovie } = useContext(MovieContext)
    const { fetchDataById } = functions

    useEffect(() => {
        if (id !== undefined) {
            fetchDataById(id)
        }
    }, [])

    return (
        <div style={{ width: "100%" }}>
            {movie !== null && (
                <>
                    <div style={{ background: "#fff", padding: "32px", marginBottom: "16px" }}>
                        <div>
                            <h2>{detailMovie.title} ({detailMovie.year})</h2>
                            <img className="fakeimg" style={{ marginLeft: "15%", width: '70%', height: '300px', objectFit: 'cover' }} alt={"Picture " + detailMovie.name} src={detailMovie.image_url} />
                            <br />
                            <br />
                            <div>
                                <strong>Duration: {(detailMovie.duration)} minutes</strong><br />
                                <strong>Rating: {detailMovie.rating}</strong><br />
                                <strong>Genre: {(detailMovie.genre)}</strong><br />
                                <br />
                            </div>
                            <p>
                                <strong style={{ marginRight: '10px' }}>Description :</strong>
                                {detailMovie.description}
                            </p>
                            <Link to={`/`}>
                                <Button type="primary" ><LeftOutlined />Back</Button>
                            </Link>
                            <br />
                        </div>
                    </div>


                </>
            )}
        </div>
    )

}

export default MovieDetail