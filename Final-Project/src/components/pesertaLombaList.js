import React, { useContext, useEffect } from "react"
import { Link } from "react-router-dom"
import { PesertaLombaContext } from "./pesertaLombaContext"

import { Button, Table, Tag, Space } from "antd"

const PesertaLombaList = () => {

    const { pesertaLomba, functions, fetchStatus, setFetchStatus } = useContext(PesertaLombaContext)
    const { fetchData, functionDelete, functionEdit } = functions

    useEffect(() => {

        fetchData()

    }, [])

    const handleDelete = (event) => {
        let idPeserta = parseInt(event.target.value)
        console.log(idPeserta)
        functionDelete(idPeserta)
    }


    const handleEdit = (event) => {
        let idPeserta = parseInt(event.target.value)
        functionEdit(idPeserta)
    }

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Action',
            key: 'action',
            render: (result) => (     
                <>
                    <button onClick={handleEdit} value={result.id} >Edit</button>
                    <button onClick={handleDelete} value={result.id} style={{marginLeft:"10px"}}>Delete</button>
                </>
            ),
        },
    ];

    const data = pesertaLomba

    return (
        <>
            <h1>Daftar Peserta Lomba</h1>
            <Link to="/peserta/form"><Button type="primary">Tambah Data</Button></Link>
            <br />
            <br />
            <Table columns={columns} dataSource={data} />
            <br />
            <br />

        </>
    )
}

export default PesertaLombaList