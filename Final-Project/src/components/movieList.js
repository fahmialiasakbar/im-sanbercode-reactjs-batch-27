import React, { useContext, useEffect } from "react"
import { Link } from "react-router-dom"
import { Button, Table } from "antd"
import { MovieContext } from "./movieContext"
import { PlusOutlined, EditOutlined, DeleteOutlined, StarFilled } from '@ant-design/icons'

const MovieList = () => {
    const { movie, functions } = useContext(MovieContext)
    const { fetchData, functionDelete, functionEdit } = functions

    useEffect(() => {
        fetchData()
    }, [])

    const handleDelete = (event) => {
        let idMovie = parseInt(event.target.value)
        if(event.target.tagName === 'SPAN'){
            idMovie = event.target.parentNode.value
            console.log(event)
        }
        console.log(idMovie)
        functionDelete(idMovie)
    }


    const handleEdit = (event) => {
        console.log(event)
        let idMovie = parseInt(event.target.value)
        console.log("Movie " + idMovie)
        functionEdit(idMovie)
    }

    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            sorter: (a, b) => a.title.localeCompare(b.title),
            filterSearch: true,
        },{
            title: 'Image',
            key: 'description',
            render: (result) => (     
                <>
                    <img src={result.image_url} alt={`${result.title}`} width="100"/>
                </>
            )
        },{
            title: 'Year',
            dataIndex: 'year',
            key: 'year',
            sorter: (a, b) => a.year - b.year,
            filterSearch: true,
        },{
            title: 'Duration',
            dataIndex: 'duration',
            key: 'duration',
            sorter: (a, b) => a.duration - b.duration
        },{
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
            sorter: (a, b) => a.genre.localeCompare(b.genre)
        },{
            title: 'Rating',
            key: 'rating',
            sorter: (a, b) => a.rating - b.rating,
            render: (result) => (     
                <>
                    <StarFilled style={{color: "#FFD700"}}/> {result.rating}
                </>
            )
        },
        {
            title: 'Action',
            key: 'action',
            render: (result) => (     
                <>
                    <Link to={`/movie/edit/${result.id}`}>
                        <Button type="primary" onClick={handleEdit} value={result.id} style={{width: "100px", background: "#ffc107 ", borderColor: "#ffc107"}}><EditOutlined />Edit</Button><br/>
                    </Link>
                    <Button type="primary" onClick={(e) => {handleDelete(e)}} value={result.id} style={{width:"100px", background: "#dc3545", borderColor: "#dc3545"}}><DeleteOutlined />Delete</Button>
                </>
            ),
        },
    ];

    const data = movie

    return (
        <>
            <br/>
            <h1>Movie List</h1>
            <Link to="/movie/add"><Button type="primary"><PlusOutlined />Add Movie</Button></Link>
            <br />
            <br />
            <Table columns={columns} dataSource={data} />
            <br />
            <br />

        </>
    )
}

export default MovieList