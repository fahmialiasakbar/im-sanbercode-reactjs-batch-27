import axios from "axios"
import Cookies from "js-cookie"
import React, { createContext, useState } from "react"
import { useHistory } from "react-router-dom"
import { Form } from "antd"
export const GameContext = createContext()

export const GameProvider = props => {
    const [form] = Form.useForm()
    let history = useHistory()
    const [game, setGame] = useState([])

    const [detailGame, setDetailGame] = useState({})
    const [fetchStatus, setFetchStatus] = useState(false)

    const fetchData = async () => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let data = result.data
        let output = data.map((e) => {
            return {
                id: e.id,
                name: e.name,
                release: e.release,
                platform: e.platform,
                genre: e.genre,
                singlePlayer: e.singlePlayer,
                multiplayer: e.multiplayer,
                image_url: e.image_url
            }
        })
        setGame(output)
        console.log(game)
    }

    const fetchData2 = async () => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let data = result.data
        let output = data.map((e) => {
            return {
                id: e.id,
                name: e.name,
                release: e.release,
                platform: e.platform,
                genre: e.genre,
                singlePlayer: e.singlePlayer,
                multiplayer: e.multiplayer,
                image_url: e.image_url
            }
        })
        setGame(output)
        console.log(game)
    }

    const fetchDataById = async (idGame) => {
        console.log("Jancok " + idGame)
        axios.get(`http://backendexample.sanbercloud.com/api/data-game/${idGame}`)
            .then((res) => {
                let data = res.data
                setDetailGame({
                    id: data.id,
                    name: data.name,
                    release: data.release,
                    platform: data.platform,
                    genre: data.genre,
                    singlePlayer: data.singlePlayer,
                    multiplayer: data.multiplayer,
                    image_url: data.image_url
                })
                form.setFieldsValue({
                    id: data.id,
                    name: data.name,
                    release: data.release,
                    platform: data.platform,
                    genre: data.genre,
                    singlePlayer: data.singlePlayer,
                    multiplayer: data.multiplayer,
                    image_url: data.image_url
                })

            })
    }


    const functionDelete = (idGame) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, {
            headers: {
                "Authorization": "Bearer " + Cookies.get('token')
            }
        })
            .then(() => {
                let newGame = game.filter((e) => { return e.id !== idGame })
                setGame(newGame)
            })
    }

    const functionSubmit = (data) => {
        axios.post(`https://backendexample.sanbersy.com/api/data-game`,
            {
                name: data.name,
                release: data.release,
                platform: data.platform,
                genre: data.genre,
                singlePlayer: data.singlePlayer,
                multiplayer: data.multiplayer,
                image_url: data.image_url
            },
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        )
            .then((res) => {
                history.push('/game')
            })
    }

    const functionUpdate = (id, data) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-game/${id}`,
            {
                name: data.name,
                release: data.release,
                platform: data.platform,
                genre: data.genre,
                singlePlayer: data.singlePlayer,
                multiplayer: data.multiplayer,
                image_url: data.image_url
            },
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        )
            .then((res) => {
                history.push('/game')
            })
    }

    const functionEdit = (id) => {
        history.push(`/game/edit/${id}`)
    }

    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit,
        fetchDataById
    }

    return (
        <GameContext.Provider value={{
            form,
            game,
            setGame,
            detailGame,
            setDetailGame,
            fetchData2,
            functions,
            fetchStatus, setFetchStatus
        }}>
            {props.children}
        </GameContext.Provider>
    )
}