import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom"

import { Layout } from 'antd';
import LayoutComponent from './layout';
import Home from './Home';

import { UserProvider } from '../context/UserContext';
import Login from '../auth/Login';
import Cookies from 'js-cookie';
import Register from '../auth/Register';

import { MovieProvider } from './movieContext';
import MovieList from './movieList';
import MovieDetail from './MovieDetail';
import MovieEdit from './MovieEdit';
import MovieAdd from './MovieAdd';

import { GameProvider } from './gameContext';
import GameList from './GameList';
import GameDetail from './GameDetail';
import GameEdit from './GameEdit';
import GameAdd from './GameAdd';

const Routes = () => {

    const LoginRoute = ({ ...props }) => {
        if (Cookies.get('token') !== undefined) {
            return <Redirect path="/" />
        } else {
            return <Route {...props} />
        }
    }


    return (
        <>
            <Router>
                <Layout className="layout">
                    <UserProvider>
                        <MovieProvider>
                            <GameProvider>
                                <Switch>
                                    <Route path="/" exact >
                                        <LayoutComponent content={<Home />} />
                                    </Route>

                                    <LoginRoute path="/login" exact >
                                        <LayoutComponent content={<Login />} />
                                    </LoginRoute>

                                    <LoginRoute path="/register" exact >
                                        <LayoutComponent content={<Register />} />
                                    </LoginRoute>


                                    <Route path="/movie" exact>
                                        <LayoutComponent content={<MovieList />} />
                                    </Route>

                                    <Route path="/movie/add/" exact >
                                        <LayoutComponent content={<MovieAdd />} />
                                    </Route>

                                    <Route path="/movie/:id" exact >
                                        <LayoutComponent content={<MovieDetail />} />
                                    </Route>

                                    <Route path="/movie/edit/:id" exact >
                                        <LayoutComponent content={<MovieEdit />} />
                                    </Route>


                                    <Route path="/game" exact>
                                        <LayoutComponent content={<GameList />} />
                                    </Route>

                                    <Route path="/game/add/" exact >
                                        <LayoutComponent content={<GameAdd />} />
                                    </Route>

                                    <Route path="/game/:id" exact >
                                        <LayoutComponent content={<GameDetail />} />
                                    </Route>

                                    <Route path="/game/edit/:id" exact >
                                        <LayoutComponent content={<GameEdit />} />
                                    </Route>
                                </Switch>
                            </GameProvider>
                        </MovieProvider>
                    </UserProvider>
                </Layout>
            </Router>
        </>
    );
}

export default Routes;