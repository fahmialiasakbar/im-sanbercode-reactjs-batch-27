import axios from "axios"
import Cookies from "js-cookie"
import React, { createContext, useState } from "react"
import { useHistory } from "react-router-dom"
import { Form } from "antd"
export const MovieContext = createContext()

export const MovieProvider = props => {
    let history = useHistory()

    const [form] = Form.useForm()
    const [movie, setMovie] = useState([])
    const [detailMovie, setDetailMovie] = useState({})
    const [fetchStatus, setFetchStatus] = useState(false)

    const fetchData = async () => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let data = result.data
        let output = data.map((e) => {
            return {
                id: e.id,
                title: e.title,
                description: e.description,
                year: e.year,
                duration: e.duration,
                genre: e.genre,
                rating: e.rating,
                review: e.review,
                image_url: e.image_url
            }
        })
        setMovie(output)
        console.log(movie)
    }
    
    const fetchDataById = async (idMovie) => {
        console.log("Jancok " + idMovie)
        axios.get(`http://backendexample.sanbercloud.com/api/data-movie/${idMovie}`)
            .then((res) => {
                let data = res.data
                setDetailMovie({    
                    id: data.id,
                    title: data.title,
                    description: data.description,
                    year: data.year,
                    duration: data.duration,
                    genre: data.genre,
                    rating: data.rating,
                    review: data.review,
                    image_url: data.image_url
                })
                form.setFieldsValue({
                    id: data.id,
                    title: data.title,
                    description: data.description,
                    year: data.year,
                    duration: data.duration,
                    genre: data.genre,
                    rating: data.rating,
                    review: data.review,
                    image_url: data.image_url      
                })
    
            })
    }


    const functionDelete = (idMovie) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`, {
            headers: {
                "Authorization": "Bearer " + Cookies.get('token')
            }
        })
            .then(() => {
                
                let newMovie = movie.filter((e) => { return e.id !== idMovie })
                setMovie(newMovie)

            })
    }

    const functionSubmit = (data) => {
        axios.post(`https://backendexample.sanbersy.com/api/data-movie`,
            { 
                title: data.title,
                description: data.description,
                year: data.year,
                duration: data.duration,
                genre: data.genre,
                rating: data.rating,
                review: data.review,
                image_url: data.image_url
            },
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        )
            .then((res) => {
                history.push('/movie')
            })
    }

    const functionUpdate = (id, data) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-movie/${id}`,
            { 
                title: data.title,
                description: data.description,
                year: data.year,
                duration: data.duration,
                genre: data.genre,
                rating: data.rating,
                review: data.review,
                image_url: data.image_url
            },
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        )
            .then((res) => {
                history.push('/movie')
            })
    }

    const functionEdit = (id) => {
        history.push(`/movie/edit/${id}`)
    }

    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit,
        fetchDataById
    }

    return (
        <MovieContext.Provider value={{
            form,
            movie,
            setMovie,
            detailMovie,
            setDetailMovie,
            functions,
            fetchStatus, setFetchStatus
        }}>
            {props.children}
        </MovieContext.Provider>
    )
}