import React, { useContext } from "react"
import { Link, useHistory } from "react-router-dom"
import { Layout, Menu } from 'antd';
import { UserContext } from "../context/UserContext";
import Cookies from "js-cookie";

const { Header } = Layout;

const Nav = () => {
    let history = useHistory()
    const { loginStatus, setLoginStatus } = useContext(UserContext)

    const handleLogout = () => {
        setLoginStatus(false)
        Cookies.remove('token')
        Cookies.remove('name')
        Cookies.remove('email')
        history.push('/login')
    }

    return (
        <>
            <Header>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                    <Menu.Item key={"1"}><Link to="/">Home</Link></Menu.Item>
                    {
                        Cookies.get('token') !== undefined && <Menu.Item key={"2"}><Link to="/movie">Movies</Link></Menu.Item>
                    }
                    {
                        Cookies.get('token') !== undefined && <Menu.Item key={"3"}><Link to="/game">Games</Link></Menu.Item>
                    }
                    {
                        Cookies.get('token') !== undefined && <Menu.Item key={"4"} style={{ position: "absolute", right: "0" }} onClick={handleLogout}><span >Logout</span></Menu.Item>
                    }

                    {
                        Cookies.get('token') === undefined && (
                            <>
                                <Menu.Item key={"2"} style={{ position: "absolute", right: "80px" }}><Link to="/register">Register</Link></Menu.Item>
                                <Menu.Item key={"3"} style={{ position: "absolute", right: "0" }}><Link to="/login">Login</Link></Menu.Item>
                            </>
                        )
                    }
                </Menu>
            </Header>
        </>
    )
}

export default Nav