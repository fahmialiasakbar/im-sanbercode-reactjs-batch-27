import React, { useContext } from "react"
import { Button, Form, Input, InputNumber, Checkbox } from "antd"
import { GameContext } from "./gameContext"


const GameAdd = () => {

    const { functions } = useContext(GameContext)
    const { functionSubmit } = functions

    const handleSubmit = (data) => {
        functionSubmit(data)
    }

    return (
        <div style={{ padding: "32px", background: "#fff" }}>
            <Form
                style={{ width: "500px" }}
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                initialValues={{
                }}
                onFinish={handleSubmit}
                autoComplete="off"
            >
                <Form.Item
                    label="Name"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the name!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    label="Release"
                    name="release"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the release!',
                        },
                    ]}
                >
                    <InputNumber
                        min={2000}
                        precision={0}
                        step={1}
                        max={2021} />
                </Form.Item>


                <Form.Item
                    label="Platform"
                    name="platform"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the platform!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    label="Genre"
                    name="genre"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the genre!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="singlePlayer"
                    valuePropName="checked"
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Checkbox>Single Player</Checkbox>
                </Form.Item>


                <Form.Item
                    name="multiPlayer"
                    valuePropName="checked"
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Checkbox>Multi Player</Checkbox>
                </Form.Item>


                <Form.Item
                    label="Image Url"
                    name="image_url"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the image url!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}

export default GameAdd