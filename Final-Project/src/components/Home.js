import React, { useContext, useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Button } from "antd"
import { MovieContext } from "./movieContext"
import { GameContext } from "./gameContext"

const Home = () => {
    const [show, setShow] = useState("movie")
    const { movie, functions } = useContext(MovieContext)
    const { game, fetchData2 } = useContext(GameContext)
    const { fetchData } = functions

    const handleRadio = (e) => {
        setShow(e.target.value)
    }

    useEffect(() => {
        fetchData()
        fetchData2()
    }, [])

    return (
        <div style={{ width: "800px" }}>
            <div style={{textAlign: "center"}}>
                <input type="radio" name="show" value="movie" onChange={handleRadio}/> Movie &nbsp;
                <input type="radio" name="show" value="game" onChange={handleRadio} /> Game
            </div>

            {movie !== null && show === "movie" && (
                <>
                    {movie.map((item, index) => {
                        return (
                            <div style={{ background: "#fff", padding: "32px", marginBottom: "16px" }}>
                                <div>
                                    <h2>{item.title} ({item.year})</h2>
                                    <img className="fakeimg" style={{ width: '50%', height: '300px', objectFit: 'cover' }} alt={"Picture " + item.name} src={item.image_url} />
                                    <br />
                                    <br />
                                    <div>
                                        <strong>Duration: {(item.duration)} minutes</strong><br />
                                        <strong>Rating: {item.rating}</strong><br />
                                        <strong>Genre: {(item.genre)}</strong><br />
                                        <br />
                                    </div>
                                    <p>
                                        <strong style={{ marginRight: '10px' }}>Description :</strong>
                                        {item.description}
                                    </p>
                                    <Link to={`/movie/${item.id}`}>
                                        <Button type="primary" >More</Button>
                                    </Link>
                                    <br />
                                </div>
                            </div>

                        )
                    })}
                </>
            )}

            {movie !== null && show === "game" && (
                <>
                    {game.map((item, index) => {
                        return (
                            <div style={{ background: "#fff", padding: "32px", marginBottom: "16px" }}>
                                <div>
                                    <h2>{item.name} ({item.release})</h2>
                                    <img className="fakeimg" style={{ width: '50%', height: '300px', objectFit: 'cover' }} alt={"Picture " + item.name} src={item.image_url} />
                                    <br />
                                    <br />
                                    <div>
                                        <strong>Platform: {(item.platform)}</strong><br />
                                        <strong>Genre: {(item.genre)}</strong><br />
                                        <br />
                                    </div>
                                    <Link to={`/game/${item.id}`}>
                                        <Button type="primary" >More</Button>
                                    </Link>
                                    <br />
                                </div>
                            </div>

                        )
                    })}
                </>
            )}
        </div>
    )

}

export default Home