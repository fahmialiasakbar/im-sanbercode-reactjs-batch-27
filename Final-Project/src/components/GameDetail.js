import React, { useContext, useEffect } from "react"
import { Link } from "react-router-dom"
import { Button } from "antd"
import { GameContext } from "./gameContext"
import { LeftOutlined, CheckOutlined } from '@ant-design/icons'
import { useParams } from "react-router-dom"

const GameDetail = () => {
    let { id } = useParams()

    const { game, functions, detailGame } = useContext(GameContext)
    const { fetchDataById } = functions

    useEffect(() => {
        if (id !== undefined) {
            fetchDataById(id)
        }
    }, [])

    return (
        <div style={{ width: "100%" }}>
            {game !== null && (
                <>
                    <div style={{ background: "#fff", padding: "32px", marginBottom: "16px" }}>
                        <div>
                            <h2>{detailGame.name} ({detailGame.release})</h2>
                            <img className="fakeimg" style={{ marginLeft: "15%", width: '70%', height: '300px', objectFit: 'cover' }} alt={"Picture " + detailGame.name} src={detailGame.image_url} />
                            <br />
                            <br />
                            <div>
                                <strong>Platform: {(detailGame.platform)}</strong><br />
                                <strong>Genre: {(detailGame.genre)}</strong><br />
                                <br />
                            </div>
                            <>
                                {detailGame.singlePlayer === 1 &&
                                    <span>
                                        <CheckOutlined style={{ color: "#008800" }} /> Single Player
                                        <br />
                                    </span>
                                }
                                {detailGame.multiplayer === 1 &&
                                    <span>
                                        <CheckOutlined style={{ color: "#008800" }} /> Multi Player
                                        <br />
                                    </span>
                                }
                                <br />
                            </>
                            <Link to={`/`}>
                                <Button type="primary" ><LeftOutlined />Back</Button>
                            </Link>
                            <br />
                        </div>
                    </div>
                </>
            )}
        </div>
    )

}

export default GameDetail