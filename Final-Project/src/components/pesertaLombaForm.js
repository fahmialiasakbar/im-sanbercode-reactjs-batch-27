import React, { useContext, useEffect } from "react"
import { useParams } from "react-router-dom"
import { PesertaLombaContext } from "./pesertaLombaContext"

const PesertaLombaForm = () => {

    let { Id } = useParams()
    console.log(Id)
    const { inputName, setInputName, currentIndex, setCurrentIndex, functions } = useContext(PesertaLombaContext)
    const { functionSubmit, functionUpdate, fetchDataById } = functions

    useEffect(() => {
        if( Id !== undefined ){
            fetchDataById(Id)
        }
    },[])
    
    const handleChange = (event) => {
        setInputName(event.target.value);
    }


    const handleSubmit = (event) => {
        event.preventDefault()

        if (currentIndex === -1) {
            functionSubmit()
        } else {
            functionUpdate()
        }

        setInputName("")
        setCurrentIndex(-1)
    }
    return (
        <>
            <h1>Form Peserta Lomba</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    Masukkan nama peserta:
                </label>
                <input type="text" value={inputName} onChange={handleChange} />
                <button>submit</button>
            </form>
        </>
    )
}

export default PesertaLombaForm