import React, { useContext, useEffect } from "react"
import { Button, Form, Input, InputNumber } from "antd"
import { MovieContext } from "./movieContext"
import { useParams } from "react-router-dom"


const MovieEdit = () => {
    const { form, functions, detailMovie, setDetailMovie } = useContext(MovieContext)
    const { functionDelete, functionEdit, fetchDataById, functionUpdate } = functions
    let { id } = useParams()

    useEffect(() => {
        if (id !== undefined) {
            fetchDataById(id)
        }
    }, [])

    const handleImage = (event) => {
        setDetailMovie((e) => {
            e.image_url = event.target.value
            return e
        })
        console.log(detailMovie)
    }

    const handleSubmit = (data) => {
        functionUpdate(id, data)
    }

    return (
        <div style={{ padding: "32px", background: "#fff", marginTop: "16px" }}>
            <Form
                style={{ width: "500px" }}
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                onFinish={handleSubmit}
                autoComplete="off"
                form={form}
            >
                <Form.Item
                    label="Title"
                    name="title"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the title!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    label="Description"
                    name="description"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the description!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    label="Year"
                    name="year"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the year!',
                        },
                    ]}
                >
                    <InputNumber
                        min={1980}
                        precision={0}
                        step={1}
                        max={2021} />
                </Form.Item>


                <Form.Item
                    label="Duration"
                    name="duration"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the duration!',
                        },
                    ]}
                >
                    <InputNumber />
                </Form.Item>


                <Form.Item
                    label="Genre"
                    name="genre"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the genre!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    label="Rating"
                    name="rating"
                    type="number"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the rating!',
                        },
                    ]}
                >
                    <InputNumber
                        min={0}
                        precision={0}
                        step={1}
                        max={10} />

                </Form.Item>


                <Form.Item
                    label="Review"
                    name="review"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the review!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    label="Image Url"
                    name="image_url"
                    rules={[
                        {
                            required: true,
                            message: 'Please fill the image url!',
                        },
                    ]}
                >
                    <Input onChange={handleImage} />
                </Form.Item>


                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
            <img src={`${detailMovie.image_url}`} alt={detailMovie.title} style={{ width: "350px", position: "absolute", right: "100px", top: "150px" }} />
        </div>
    )
}

export default MovieEdit