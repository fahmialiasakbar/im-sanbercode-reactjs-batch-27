import React from "react"
import Nav from "./Nav"

import { Layout } from 'antd';

const { Content, Footer } = Layout;

const LayoutComponent = (props) => {
    return (
        <>
            <Nav />
            <Content style={{ padding: '20px 50px' }}>
                <div className="site-layout-content" style={{ minHeight: "100vh" }}>
                    {props.content}
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Fahmi Akbar @2021</Footer>
        </>
    )
}

export default LayoutComponent