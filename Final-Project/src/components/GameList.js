import React, { useContext, useEffect } from "react"
import { Link } from "react-router-dom"
import { Button, Table, Tag, Space } from "antd"
import { GameContext } from "./gameContext"
import { PlusOutlined, EditOutlined, DeleteOutlined, CheckOutlined } from '@ant-design/icons'

const GameList = () => {
    const { game, functions } = useContext(GameContext)
    const { fetchData, functionDelete, functionEdit } = functions

    useEffect(() => {
        fetchData()
    }, [])

    const handleDelete = (event) => {
        let idGame = parseInt(event.target.value)
        if (event.target.tagName === 'SPAN') {
            idGame = event.target.parentNode.value
            console.log(event)
        }
        console.log(idGame)
        functionDelete(idGame)
    }


    const handleEdit = (event) => {
        console.log(event)
        let idGame = parseInt(event.target.value)
        console.log("Game " + idGame)
        functionEdit(idGame)
    }

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name.localeCompare(b.name)
        }, {
            title: 'Image',
            key: 'image_url',
            render: (result) => (
                <>
                    <img src={result.image_url} alt={`${result.title}`} width="100" />
                </>
            )
        }, {
            title: 'Release',
            dataIndex: 'release',
            key: 'release',
            sorter: (a, b) => a.release - b.release,
        }, {
            title: 'Platform',
            dataIndex: 'platform',
            key: 'platform',
        }, {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
            sorter: (a, b) => a.genre.localeCompare(b.genre)
        }, {
            title: 'Single Player',
            render: (result) => (
                <>
                    {result.singlePlayer === 1 &&
                        <CheckOutlined style={{ color: "#008800" }} />
                    }
                </>
            )
        }, {
            title: 'Multi Player',
            render: (result) => (
                <>
                    {result.multiplayer === 1 &&
                        <CheckOutlined style={{ color: "#008800" }} />
                    }
                </>
            )
        },
        {
            title: 'Action',
            key: 'action',
            render: (result) => (
                <>
                    <Link to={`/game/edit/${result.id}`}>
                        <Button type="primary" onClick={handleEdit} value={result.id} style={{ width: "100px", background: "#ffc107 ", borderColor: "#ffc107" }}><EditOutlined />Edit</Button><br />
                    </Link>
                    <Button type="primary" onClick={(e) => { handleDelete(e) }} value={result.id} style={{ width: "100px", background: "#dc3545", borderColor: "#dc3545" }}><DeleteOutlined />Delete</Button>
                </>
            ),
        },
    ];

    const data = game
    console.log(game)

    return (
        <>
            <br />
            <h1>Game List</h1>
            <Link to="/game/add"><Button type="primary"><PlusOutlined />Add Game</Button></Link>
            <br />
            <br />
            <Table columns={columns} dataSource={data} />
            <br />
            <br />

        </>
    )
}

export default GameList