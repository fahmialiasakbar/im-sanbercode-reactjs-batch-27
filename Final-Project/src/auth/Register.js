import React, { useContext, useState } from "react"
import axios from "axios"
import Cookies from "js-cookie"
import { useHistory } from "react-router"
import { Form, Input, Button } from 'antd';
const Register = () => {
    let history = useHistory()
    const handleSubmit = (data) => {
        // event.preventDefault()
        // console.log(input)
        axios.post(`https://backendexample.sanbersy.com/api/register`,{
            name : data.username,
            email : data.email,
            password : data.password
        }).then((res) => {
            console.log(res)
            history.push(`/login`)
        })
    }

    return (
        <Form
            style={{width: "500px"}}
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={handleSubmit}
          autoComplete="off"
        >
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your email!',
              },
            ]}
          >
            <Input/>
          </Form.Item>

          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input/>
          </Form.Item>
    
          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password/>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      )
}

export default Register