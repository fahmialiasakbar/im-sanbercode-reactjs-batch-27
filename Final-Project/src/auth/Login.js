import axios from "axios"
import Cookies from "js-cookie"
import React, { useContext, useState } from "react"
import { useHistory } from "react-router"
import { UserContext } from "../context/UserContext"
import { Form, Input, Checkbox, Button } from 'antd';
const Login = () => {
    let history = useHistory()
    const { setLoginStatus } = useContext(UserContext)

    const [input, setInput] = useState({
        email: "",
        password: ""
    })

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInput({...input, [name] : value})
    }

    const handleSubmit = (data) => {
        // event.preventDefault()
        
        axios.post(`https://backendexample.sanbersy.com/api/user-login`,{
            email : data.email,
            password : data.password
        }).then((res) => {
            let token = res.data.token
            let user = res.data.user

            Cookies.set('token', token, {expires: 1})
            Cookies.set('name', user.name, {expires: 1})
            Cookies.set('email', user.email, {expires: 1})
            history.push('/')
        }).catch((err)=>{
            console.log(err.message)
        })
    }

    
  return (
    <Form
        style={{width: "500px"}}
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={handleSubmit}
      autoComplete="off"
    >
      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            required: true,
            message: 'Please input your email!',
          },
        ]}
      >
        <Input/>
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password/>
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
}

export default Login