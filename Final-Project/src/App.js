import React from 'react';
import Routes from './components/Routes';
import 'antd/dist/antd.css';

const App = () => {
    return(
        <>
            <Routes/>
        </>
    )
}

export default App;