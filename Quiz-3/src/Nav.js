import React, { useContext } from "react"
import { useHistory } from "react-router-dom"
import './assets/css/style.css'
import { Input } from 'antd'
import logo from './assets/img/logo.png'
const { Search } = Input;

const Nav = () =>{
    let history = useHistory()
    const onSearch = (value) => {
        history.push(`/search/${value}`)
    }
    
  return(

    <div className="topnav">
        <a href="">
            <img src={logo} width="70" />
        </a>
        <a href="/">Home</a>
        <a href="/mobile-list">Movie List</a>
        <a href="/about">About</a>
        <form>
                <Search placeholder="Search" onSearch={onSearch} enterButton />
        </form>
    </div>
    )
}

export default Nav