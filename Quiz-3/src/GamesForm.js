import React, { useContext, useEffect } from "react"
import { useHistory, useParams } from "react-router-dom"
import { GamesContext } from "./Context"
import { Input, Space } from 'antd'
const GamesForm = () => {
    let { Id } = useParams()
    console.log(Id)
    const { input, setInput, currentId, setCurrentId, functions } = useContext(GamesContext)
    const { functionSubmit, functionUpdate, fetchDataById } = functions

    useEffect(() => {
        if (Id !== undefined) {
            fetchDataById(Id)
        }
    }, [])

    const handleChange = (event) => {
        // console.log(event.target.value)
        let typeOfValue = event.target.value
        let name = event.target.name
        if (event.target.type === 'checkbox') {
            typeOfValue = event.target.checked
            if (typeOfValue === true)
                typeOfValue = 1
            else
                typeOfValue = 0
        }
        setInput({ ...input, [name]: typeOfValue })
        console.log(input)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(input)

        if (currentId === -1) {
            functionSubmit()
        } else {
            functionUpdate(currentId)
        }

        setInput({
            name: "",
            description: "",
            category: "",
            release_year: 0,
            size: 0,
            price: 0,
            rating: 0,
            image_url: "",
            is_android_app: 0,
            is_ios_app: 0
        })
        setCurrentId(-1)
    }

    return (
        <>

            <div className="row">
                <div className="section">
                    <h1 style={{ textAlign: "center", marginTop: "50px" }}>Mobile Apps Form</h1>
                    <br />
                    <form method="post" onSubmit={handleSubmit} style={{ width: "50%", margin: "auto", padding: "50px", marginBottom: "20px" }}>
                        <strong style={{ width: "300px", display: "inline-block" }}>Nama : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.name} name="name" type="text" required />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Description : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.description} name="description" type="text" required />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Release Year : </strong>
                        <br />
                        <input style={{ float: "right" }} defaultValue="2007" min="2007" onChange={handleChange} value={input.release_year} name="release_year" type="number" required />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Size : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.size} name="size" type="number" />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Price : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.price} name="price" type="number" />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Rating : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.rating} name="rating" type="number" min="0" max="5" required />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Image Url : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.image_url} name="image_url" type="text" required />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Android App : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.is_android_app} name="is_android_app" type="checkbox" />
                        <br />
                        <strong style={{ width: "300px", display: "inline-block" }}>Ios App : </strong>
                        <br />
                        <input style={{ float: "right" }} onChange={handleChange} value={input.is_ios_app} name="is_ios_app" type="checkbox" />
                        <br />
                        <button style={{ float: "right" }} type="submit" />
                    </form>
                </div>
            </div>
        </>
    )
}

export default GamesForm