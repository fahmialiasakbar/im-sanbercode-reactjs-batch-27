import React from "react";

const About = () => {
    return (
        <div className="row">
            <div className="section">
                <h1 style={{ textAlign: 'center' }}>Data Peserta Jabarcodingcamp-2021 Reactjs</h1>
                <ol>
                    <li><strong style={{ width: '100px' }}>Nama:</strong> Fahmi Akbar</li>
                    <li><strong style={{ width: '100px' }}>Email:</strong> fahmialiasakbar@gmail.com</li>
                    <li><strong style={{ width: '100px' }}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                    <li><strong style={{ width: '100px' }}>Akun Gitlab:</strong> fahmialiasakbar</li>
                    <li><strong style={{ width: '100px' }}>Akun Telegram:</strong> fahmialiasakbar</li>
                </ol>
            </div>
        </div>
    )
}

export default About