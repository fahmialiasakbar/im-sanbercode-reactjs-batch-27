import axios from "axios"
import React, { createContext, useState } from "react"
import { useHistory } from "react-router-dom"

export const GamesContext = createContext()

export const GamesProvider = props => {
    let history = useHistory()
    const [dataGames, setDataGames] = useState([])
    const [input, setInput] = useState({
        nama: "",
        course: "",
        score: 0
    })
    const [currentId, setCurrentId] = useState(-1)
    const [fetchStatus, setFetchStatus] = useState(false)


    const fetchData = async () => {
        let result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
        setDataGames(result.data)
    }

    const fetchDataById = async (id) => {
        let result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`)
        let data = result.data
        setInput({
            id: data.id,
            name: data.name,
            description: data.description,
            category: data.category,
            release_year: data.release_year,
            size: data.size,
            price: data.price,
            rating: data.rating,
            image_url: data.image_url,
            is_android_app: data.is_android_app,
            is_ios_app: data.is_ios_app
        })
        setCurrentId(data.id)
    }

    const getScore = (score) => {
        if (score >= 80) {
            return "A"
        } else if (score >= 70 && score < 80) {
            return "B"
        } else if (score >= 60 && score < 70) {
            return "C"
        } else if (score >= 50 && score < 60) {
            return "D"
        } else {
            return "E"
        }
    }

    const functionSubmit = () => {
        console.log(input)
        axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
            name: input.name,
            description: input.description,
            category: input.category,
            release_year: input.release_year,
            size: input.size,
            price: input.price,
            rating: input.rating,
            image_url: input.image_url,
            is_android_app: input.is_android_app,
            is_ios_app: input.is_ios_app
        }).then((res) => {
            let data = res.data
            setDataGames([...dataGames, {
                name: data.name,
                description: data.description,
                category: data.category,
                release_year: data.release_year,
                size: data.size,
                price: data.price,
                rating: data.rating,
                image_url: data.image_url,
                is_android_app: data.is_android_app,
                is_ios_app: data.is_ios_app
            }])
            history.push('/mobile-list')
            // history.push('/tugas14')
        })
    }

    const functionUpdate = (currentId) => {
        console.log(input)
        axios.put(`http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`, {
            name: input.name,
            description: input.description,
            category: input.category,
            release_year: input.release_year,
            size: input.size,
            price: input.price,
            rating: input.rating,
            image_url: input.image_url,
            is_android_app: input.is_android_app,
            is_ios_app: input.is_ios_app
        }).then((res) => {
            // let newGames = dataGames.find((e) => e.id === currentId)
            // console.log(newGames)
            // newGames.name = input.name
            // newGames.description = input.description
            // newGames.category = input.category
            // newGames.release_year = input.release_year
            // newGames.size = input.size
            // newGames.price = input.price
            // newGames.rating = input.rating
            // newGames.image_url = input.image_url
            // newGames.is_android_app = input.is_android_app
            // newGames.is_ios_app = input.is_ios_app
            // setDataGames([...dataGames])
            history.push('/mobile-list')

        })
    }

    const functionDelete = (idGames) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${idGames}`)
            .then(() => {
                let newDataMahasiswa = dataGames.filter((res) => { return res.id !== idGames })
                setDataGames(newDataMahasiswa)
            })
    }

    const functionEdit = (idGames) => {

        history.push(`/mobile-form/edit/${idGames}`)

    }

    const functions = {
        fetchData,
        getScore,
        functionSubmit,
        functionUpdate,
        functionDelete,
        functionEdit,
        fetchDataById
    }

    return (
        <GamesContext.Provider value={{
            dataGames,
            setDataGames,
            input,
            setInput,
            currentId,
            setCurrentId,
            functions,
            fetchStatus,
            setFetchStatus
        }}>
            {props.children}
        </GamesContext.Provider>
    )

}