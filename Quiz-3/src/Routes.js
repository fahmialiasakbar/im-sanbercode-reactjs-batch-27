import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import Nav from "./Nav";
import { GamesProvider } from "./Context";
import Content from "./Content";
import About from "./About";
import GamesList from "./GamesList";
import GamesForm from "./GamesForm";
import Search from "./Search";
const Routes = () => {
  return (
    <>
      <Router>
        <GamesProvider>
          <Nav />
          <Switch>
            <Route exact path="/">
              <Content />
            </Route>

            <Route exact path="/mobile-list">
              <GamesList />
            </Route>

            <Route exact path="/mobile-form">
              <GamesForm />
            </Route>

            <Route exact path="/mobile-form/edit/:Id">
              <GamesForm />
            </Route>

            <Route exact path="/search/:valueOfSearch">
              <Search />
            </Route>

            <Route exact path="/about">
              <About/>
            </Route>
          </Switch>
        </GamesProvider>
      </Router>
    </>
  );
}

export default Routes