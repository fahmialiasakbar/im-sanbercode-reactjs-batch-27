import React, { useContext, useEffect } from "react"
import { Button } from 'antd';
import { GamesContext } from "./Context"
import { PlusOutlined } from '@ant-design/icons'
const GamesList = () => {

    const { dataGames, functions, fetchStatus, setFetchStatus } = useContext(GamesContext)
    const { fetchData, functionDelete, functionEdit } = functions

    useEffect(() => {

        if (fetchStatus === false) {
            fetchData()
            setFetchStatus(true)
        }

    }, [fetchData, fetchStatus, setFetchStatus])

    const getPlatform = (android = null, ios = null) => {
        if (android && ios) {
            return 'Android & iOS'
        } else if (android) {
            return 'Android'
        } else if (ios) {
            return 'iOS'
        } else {
            return '-'
        }
    }

    function shorten(str, maxLen = 100, separator = ' ') {
        if (!str) return null
        if (str.length <= maxLen) return str;
        return str.substr(0, str.lastIndexOf(separator, maxLen));
    }

    const handleDelete = (index) => {
        let idMahasiswa = parseInt(index)

        functionDelete(idMahasiswa)
    }

    const handleEdit = (index) => {

        let idGames = parseInt(index)

        functionEdit(idGames)
    }

    return (
        <>
            <div className="row">
                <div className="section">
                    <br />
                    <a href="/mobile-form">
                        <Button type="primary"><PlusOutlined />Add</Button>
                    </a>
                    <h2>Mobile Apps List</h2>
                    <table id="customers">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th style={{ width: "400px" }}>Description</th>
                                <th>Release Year</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Rating</th>
                                <th>Platform</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {dataGames !== null && (
                                <>
                                    {dataGames.map((e, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{e.name}</td>
                                                <td>{e.category}</td>
                                                <td>{shorten(e.description)}</td>
                                                <td>{e.release_year}</td>
                                                <td>{e.size}</td>
                                                <td>{e.price}</td>
                                                <td>{e.rating}</td>
                                                <td>{getPlatform(e.is_android_app, e.is_ios_app)}</td>
                                                <td>
                                                    <Button type="primary" onClick={() => { handleEdit(e.id) }} value={e.id}>Edit</Button>
                                                    <Button type="danger" onClick={() => { handleDelete(e.id) }} value={e.id} style={{ marginLeft: "10px" }}>Delete</Button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </>
                            )}
                        </tbody>
                    </table>
                    <br />
                    <br />
                </div>
            </div>
        </>
    )
}

export default GamesList