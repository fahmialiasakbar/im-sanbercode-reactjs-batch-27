import React, { useEffect, useState, useContext } from "react";
import { GamesContext } from "./Context";
import { useParams } from "react-router-dom";



const getPrice = (price) => {
    if (price === 0)
        return 'FREE'
    return price
}

const getSize = (size) => {
    if (size >= 1000) {
        return size / 1000 + ' GB'
    }
    return size + " MB"
}

const getPlatform = (is_android = 0, is_ios = 0) => {
    if (is_android && is_ios) {
        return 'Android & iOS'
    } else if (is_android) {
        return 'Android'
    } else if (is_ios) {
        return 'iOS'
    } else {
        return '-'
    }
}

const Search = () => {
    let { valueOfSearch } = useParams()

    console.log(valueOfSearch)
    const { dataGames, functions } = useContext(GamesContext)
    const { fetchData } = functions

    useEffect(() => {
        fetchData()
    }, [])

    const [dataSearch, setDataSearch] = useState([])

    const filterData = () => {
        dataGames.map((e) => {
            console.log(e)
            if(e.name.includes(valueOfSearch)){
                dataSearch.push(e)
            }
        })
    }

    filterData()

    return (
        <div className="row">
            <div className="section">
                {dataSearch !== null && (
                    <>
                        {dataSearch.map((item, index) => {
                            return (
                                <div className="card">
                                    <div>
                                        <h2>{item.name}</h2>
                                        <h5>Release Year : {item.release_year}</h5>
                                        <img className="fakeimg" style={{ width: '50%', height: '300px', objectFit: 'cover' }} alt={"Picture " + item.name} src={item.image_url} />
                                        <br />
                                        <br />
                                        <div>
                                            <strong>Price: {getPrice(item.price)}</strong><br />
                                            <strong>Rating: {item.rating}</strong><br />
                                            <strong>Size: {getSize(item.size)}</strong><br />
                                            <strong style={{ marginRight: '10px' }}>Platform : {getPlatform(item.is_android_app, item.is_ios_app)}
                                            </strong>
                                            <br />
                                        </div>
                                        <p>
                                            <strong style={{ marginRight: '10px' }}>Description :</strong>
                                            {item.description}
                                        </p>

                                        <hr />
                                    </div>
                                </div>

                            )
                        })}
                    </>
                )}
            </div>
        </div>
    )
}

export default Search