import React, { useState } from 'react';
import './crud.css';
const Crud = () => {
    const [daftarBuah, setDaftarBuah] = useState([
        { nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        { nama: "Manggis", hargaTotal: 350000, beratTotal: 10000 },
        { nama: "Nangka", hargaTotal: 90000, beratTotal: 2000 },
        { nama: "Durian", hargaTotal: 400000, beratTotal: 5000 },
        { nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000 }
    ])

    const [inputName, setInputName] = useState("")
    const [inputPrice, setInputPrice] = useState(0)
    const [inputWeight, setInputWeight] = useState(0)
    const [currentIndex, setCurrentIndex] = useState(-1)

    const handleChangeName = (event) => {
        setInputName(event.target.value);
    }
    const handleChangePrice = (event) => {
        setInputPrice(event.target.value);
    }
    const handleChangeWeight = (event) => {
        setInputWeight(event.target.value);
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if (inputName && inputPrice && inputWeight) {
            if (inputWeight >= 2000) {
                let newData = daftarBuah
                let enterData = {
                    nama: inputName,
                    hargaTotal: inputPrice,
                    beratTotal: inputWeight
                }
                if (currentIndex === -1) {
                    newData = [...daftarBuah, enterData]
                } else {
                    newData[currentIndex] = enterData
                }

                setDaftarBuah(newData)
                setCurrentIndex(-1)
                setInputName("")
                setInputPrice(0)
                setInputWeight(0)
            } else {
                alert("Berat minila 2000g / 2kg")
            }
        } else {
            alert("Anda harus mengisi semua data")
        }
    }

    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = daftarBuah[index]
        let newData = daftarBuah.filter((e) => { return e !== deletedItem })
        if(index < currentIndex){
            setCurrentIndex(currentIndex - 1)
        } else if(index == currentIndex){
            setInputName("")
            setInputPrice(0)
            setInputWeight(0)
            setCurrentIndex(-1)
        }
        setDaftarBuah(newData)
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editName = daftarBuah[index].nama
        let editPrice = daftarBuah[index].hargaTotal
        let editWeight = daftarBuah[index].beratTotal
        setInputName(editName)
        setInputPrice(editPrice)
        setInputWeight(editWeight)
        setCurrentIndex(event.target.value)
    }

    return (
        <>
            <h1>Daftar Buah</h1>
            <table className="table-buah">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga Total</th>
                        <th>Berat Total</th>
                        <th>Harga per kg</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    {
                        daftarBuah.map((val, index) => {
                            return (
                                <tr>
                                    <td className="align-center">{index + 1}</td>
                                    <td>{val.nama}</td>
                                    <td className="align-right">{val.hargaTotal}</td>
                                    <td className="align-right">{val.beratTotal / 1000} kg</td>
                                    <td className="align-right">{Math.round(val.hargaTotal / val.beratTotal * 1000)}</td>
                                    <td>
                                        <button className="btn-yellow" onClick={handleEdit} value={index}>Edit</button>
                                        <button className="btn-red" onClick={handleDelete} value={index}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }

                </tbody>
            </table>
            {/* Form */}
            <h1>Form Peserta</h1>
            <form className="form-buah" onSubmit={handleSubmit}>
                <table>
                    <tr>
                        <td width="200px">
                            Nama
                        </td>
                        <td>
                            <input type="text" value={inputName} onChange={handleChangeName} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Harga Total
                        </td>
                        <td>
                            <input type="number" value={inputPrice} onChange={handleChangePrice} />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Berat total (dalam gram)
                        </td>
                        <td>
                            <input type="number" value={inputWeight} onChange={handleChangeWeight} />
                        </td>
                    </tr>
                </table>
                <button>Submit</button>
            </form>
        </>
    )

}

export default Crud