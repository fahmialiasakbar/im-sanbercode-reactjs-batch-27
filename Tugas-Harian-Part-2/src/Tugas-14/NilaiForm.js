import React, { useContext, useEffect, useState } from "react"
import { NilaiContext } from "./NilaiContext"
import { useParams } from "react-router-dom"

const NilaiForm = () => {
    const { 
        inputName, 
        setInputName, 
        inputMK, 
        setInputMK, 
        inputNilai, 
        setInputNilai,
        currentId,
        setCurrentId,
        functions
    } = useContext(NilaiContext)

    const { functionSubmit, functionUpdate, functionEdit} = functions
    
    const handleSubmit = (event) => {
        event.preventDefault()

        if (currentId === -1) {
            functionSubmit()
        }else{
            functionUpdate()
        }

        setInputName("")
        setInputMK("")
        setInputNilai(0)
        setCurrentId(-1)
    }

    const handleChangeName = (event) => {
        let inputValue = event.target.value
        setInputName(inputValue)
    }
    const handleChangeMK = (event) => {
        let inputValue = event.target.value
        setInputMK(inputValue)
    }
    const handleChangeNilai = (event) => {
        let inputValue = event.target.value
        setInputNilai(inputValue)
    }

    let { currId } = useParams();
    
    useEffect(() => {;
        if (currId !== -1) {
            functionEdit(currId)
        }    
    }, [])

    return (
        <>
            <h1>Form Nilai</h1>
            <form className="form-student" style={{ paddingBottom: "20px" }} onSubmit={handleSubmit}>
                <table>
                    <tr>
                        <td>Nama:</td>
                        <td>
                            <input type="text" value={inputName} onChange={handleChangeName} required />
                        </td>
                    </tr>
                    <tr>
                        <td>Matakuliah:</td>
                        <td>
                            <input type="text" value={inputMK} onChange={handleChangeMK} required />
                        </td>
                    </tr>
                    <tr>
                        <td>Nilai:</td>
                        <td>
                            <input type="number" max="100" min="0" value={inputNilai} onChange={handleChangeNilai} required />
                        </td>
                    </tr>
                </table>
                <button>submit</button>
            </form>
        </>
    )
}

export default NilaiForm