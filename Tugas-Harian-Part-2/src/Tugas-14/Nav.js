import React, { useState } from "react"
import { Link } from "react-router-dom"
import './nav.css'

const Nav = () => {
    return (

        <nav>
            <ul>
                <li>
                    <Link to="/tugas9">Tugas 9</Link>
                </li>
                <li>
                    <Link to="/tugas10">Tugas 10</Link>
                </li>
                <li>
                    <Link to="/tugas11">Tugas 11</Link>
                </li>
                <li>
                    <Link to="/tugas12">Tugas 12</Link>
                </li>
                <li>
                    <Link to="/tugas13">Tugas 13</Link>
                </li>
                <li>
                    <Link to="/tugas14">Tugas 14</Link>
                </li>
            </ul>
        </nav>
    )
}

export default Nav