import React from "react"
import { NilaiProvider } from "./NilaiContext"
import NilaiForm from "./NilaiForm"
import { Link } from "react-router-dom"
import './nilai.css'

function FormNilai() {
    return (
        <NilaiProvider>
            <Link to="/tugas14">
                <button>Back to List</button>
            </Link>;
            <NilaiForm />
        </NilaiProvider>
    )
}

export default FormNilai