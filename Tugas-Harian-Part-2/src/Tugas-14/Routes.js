import React, { useState } from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom"
import Nav from './Nav'

import Todo from '../Tugas-9/tugas9'
import Timer from '../Tugas-10/tugas10'
import Crud from '../Tugas-11/Crud'
import StudentScore from '../Tugas-12/tugas12'
import Score from '../Tugas-13/Score'
import Nilai from './Nilai'
import FormNilai from './FormNilai'

const Routes = () => {

    return (
        <>
            <Router>
                <div>
                    <Nav/>
                    <Switch>
                        <Route path="/" exact component={Todo} />
                        <Route path="/tugas9" exact component={Todo} />
                        <Route path="/tugas10" exact component={Timer} />
                        <Route path="/tugas11" exact component={Crud} />
                        <Route path="/tugas12" exact component={StudentScore} />
                        <Route path="/tugas13" exact component={Score} />
                        <Route path="/tugas14" exact component={Nilai} />
                        <Route path="/form" exact component={FormNilai} />
                        <Route path="/form/:currId" exact component={FormNilai} />
                    </Switch>
                </div>
            </Router>
        </>
    );

}

export default Routes