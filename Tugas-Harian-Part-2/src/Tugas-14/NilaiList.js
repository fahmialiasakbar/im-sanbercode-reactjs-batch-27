import React, { useEffect, useContext } from "react"
import { NilaiContext } from "./NilaiContext"
import { Link } from "react-router-dom"

export const NilaiList = () => {
    const { penilaian, functions, fetchStatus, setFetchStatus } = useContext(NilaiContext)
    const { fetchData, functionDelete, functionEdit } = functions

    useEffect(() => {

        fetchData()

    }, [])
    
    const handleDelete = (event) => {
        let idStudent = parseInt(event.target.value)

        functionDelete(idStudent)

    }

    return (
        <>
            <div>
                <h1>Daftar Nilai Mahasiswa</h1>
                <table className="table-student">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Mata Kuliah</th>
                            <th>Nilai</th>
                            <th>Indeks Nilai</th>
                            <th>Akses</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            penilaian.map((item, index) => {
                                console.log(item)
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.course}</td>
                                        <td>{item.score}</td>
                                        {item.score >= 80 && <td>A</td>}
                                        {item.score >= 70 && item.score < 80 && <td>B</td>}
                                        {item.score >= 60 && item.score < 70 && <td>C</td>}
                                        {item.score >= 50 && item.score < 60 && <td>D</td>}
                                        {item.score < 50 && <td>E</td>}
                                        <td>
                                            <Link to={`/form/${item.id}`}>
                                                <button className="btn-yellow">Edit</button>
                                            </Link>
                                            &nbsp;
                                            <button className="btn-red" onClick={handleDelete} value={item.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </>
    )

}


export default NilaiList