import React, { useState, createContext, useEffect } from "react";
import axios from "axios";

export const NilaiContext = createContext();

export const NilaiProvider = props => {

    const [penilaian, setPenilaian] = useState([])

    const [inputName, setInputName] = useState("")
    const [inputMK, setInputMK] = useState("")
    const [inputNilai, setInputNilai] = useState(0)
    const [currentId, setCurrentId] = useState(-1)
    const [fetchStatus, setFetchStatus] = useState(false)

    const fetchData = async () => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
        setPenilaian(result.data.map(x => { return { id: x.id, name: x.name, course: x.course, score: x.score } }))
    }


    const functionDelete = (idStudent) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idStudent}`)
            .then(() => {
                let newPesertaLomba = penilaian.filter(el => { return el.id !== idStudent })
                setPenilaian(newPesertaLomba)
            })
    }

    const functionSubmit = (params) => {
        axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputName, course: inputMK, score: inputNilai })
            .then(res => {
                let data = res.data
                setPenilaian([...penilaian, { id: data.id, name: data.name, course: data.course, score: data.score }])
            })

    }

    const functionUpdate = (params) => {
        axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputName, course: inputMK, score: inputNilai })
            .then(() => {
                // let singlePeserta = penilaian.find(el => el.id === currentId)
                // singlePeserta.name = inputName
                // singlePeserta.course = inputMK
                // singlePeserta.score = inputNilai
                // setPenilaian([...penilaian])
            })
    }

    const functionEdit = (idStudent) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idStudent}`)
            .then(res => {
                let data = res.data
                console.log(data)
                setInputName(data.name)
                setInputMK(data.course)
                setInputNilai(data.score)
                setCurrentId(data.id)
            })
    }


    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit
    }

    return (
        <NilaiContext.Provider value={{
            penilaian,
            setPenilaian,
            inputName,
            setInputName,
            inputMK,
            setInputMK,
            inputNilai,
            setInputNilai,
            currentId,
            setCurrentId,
            functions,
            fetchStatus,
            setFetchStatus
        }}>
            {props.children}
        </NilaiContext.Provider>
    );
};