import React from "react"
import { NilaiProvider } from "./NilaiContext"
import NilaiList from "./NilaiList"
import NilaiForm from "./NilaiForm"
import { Link } from "react-router-dom"
import './nilai.css'

const Nilai = () => {
    return (
        <NilaiProvider>
            <Link to="/form">
                <button>Add data</button>
            </Link>
            <NilaiList />
        </NilaiProvider>
    )
}

export default Nilai