import logo from './logo.png';
import './tugas9.css';

function Learn(props) {
  return (
    <div className="list">
      <input type="checkbox" /><span>{props.text}</span>
    </div>
  )
}

function Todo() {
  return (
    <div className="container-form">
      <div className="Main">
        <img src={logo} alt="Logo Sanbercode" /><br />
        <h1>THINGS TO DO</h1>
        <p>During bootcamp in sanbercode</p>
        <Learn text="Belajar HTML & CSS" />
        <Learn text="Belajar JavaScript" />
        <Learn text="Belajar ReactJS Dasar" />
        <Learn text="Belajar ReactJS Advance" />
        <Learn text="Belajar HTML & CSS" />
        <button className="button-sanber">SEND</button>
      </div>
    </div>
  );
}

export default Todo;
