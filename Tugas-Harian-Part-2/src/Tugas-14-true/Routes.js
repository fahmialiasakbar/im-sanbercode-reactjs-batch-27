import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
// import Tugas10 from "../Tugas 10/tugas10";
// import Tugas11 from "../Tugas 11/tugas11";
// import Tugas12 from "../Tugas 12/tugas12";
// import NilaiMahasiswa from "../Tugas 13/nilaiMahasiswa";
// import Tugas9 from "../Tugas 9/tugas9";


import Todo from '../Tugas-9/tugas9'
import Timer from '../Tugas-10/tugas10'
import Crud from '../Tugas-11/Crud'
import StudentScore from '../Tugas-12/tugas12'
import Score from '../Tugas-13/Score'
import ScoreDesign from '../Tugas-15/ScoreDesign';

import Nav from "./Nav";
import { NilaiMahasiswaProvider } from "./nilaiMahasiswaContext";
import NilaiMahasiswaFrom from "./nilaiMahasiswaForm";
import NilaiMahasiswaList from "./nilaiMahasiswaList";
import SwitchTheme from "./SwitchTheme";
import { ThemeProvider } from "./ThemeContext";


const Routes = () => {
  const containerStyle = { margin: "0 auto", width: "80%" }

  return (
    <>
      <Router>
        <NilaiMahasiswaProvider>
          <ThemeProvider>
            <Nav />
            <Switch>
              <Route exact path="/">
                <Todo />
              </Route>

              <Route exact path="/tugas10">
                <Timer />
              </Route>

              <Route exact path="/tugas11">
                <Crud />
              </Route>

              <Route exact path="/tugas12">
                <StudentScore />
              </Route>

              <Route exact path="/tugas13">
                <Score />
              </Route>


              <Route exact path="/tugas14">
                <SwitchTheme />
                <NilaiMahasiswaList />
              </Route>

              <Route exact path="/tugas14/create">
                <NilaiMahasiswaFrom />
              </Route>

              <Route exact path="/tugas14/edit/:id">
                <NilaiMahasiswaFrom />
              </Route>

            </Switch>
          </ThemeProvider>
        </NilaiMahasiswaProvider>
      </Router>
    </>
  );
}

export default Routes