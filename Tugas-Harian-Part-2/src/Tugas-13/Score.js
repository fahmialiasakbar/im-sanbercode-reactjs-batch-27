import React from "react"
import { ScoreProvider } from "./ScoreContext"
import ScoreList from "./ScoreList"
import ScoreForm from "./ScoreForm"
import './score.css'

const Score = () => {
    return (
        <ScoreProvider>
            <ScoreList />
            <ScoreForm />
        </ScoreProvider>
    )
}

export default Score