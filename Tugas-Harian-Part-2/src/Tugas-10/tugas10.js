import React, { useState, useEffect } from 'react'
const Timer = () => {

    const [time, setTime] = useState(getTime())
    const [number, setNumber] = useState(100)
    useEffect(() => {
        if (number > 0) {
            const interval = setInterval(() => {
                setNumber(number - 1)
                setTime(getTime())
                console.log("Anjing")
            }, 1000)
            return () => clearInterval(interval)
        }
    })

    if (number > 0) {
        return (
            <div>
                <h2>Now at - {time}</h2>
                <h4>Countdown : {number}</h4>
            </div>
        );
    } else {
        return null;
    }
}

function getTime() {
    const template = 'h:m:s a'
    const date = new Date()
    let hours = date.getHours()
    let mins = date.getMinutes()
    if (mins < 10) mins = '0' + mins
    let secs = date.getSeconds()
    if (secs < 10) secs = '0' + secs
    let ampm = hours >= 12 ? 'PM' : 'AM'
    if (hours > 12) {
        hours = hours - 12
    }

    let output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs)
        .replace('a', ampm)
    return output
}
export default Timer