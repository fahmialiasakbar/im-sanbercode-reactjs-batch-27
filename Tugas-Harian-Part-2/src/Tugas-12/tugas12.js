import React, { useState, useEffect } from "react"
import axios from "axios"
import './score.css'
const StudentScore = () => {
    const [penilaian, setPenilaian] = useState([])
    const [inputName, setInputName] = useState("")
    const [inputMK, setInputMK] = useState("")
    const [inputNilai, setInputNilai] = useState(0)
    const [currentId, setCurrentId] = useState(null)

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            console.log(result)
            setPenilaian(result.data.map(x => { return { id: x.id, name: x.name, course: x.course, score: x.score } }))
        }

        fetchData()
    }, [])

    const handleEdit = (event) => {
        let idStudent = event.target.value
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idStudent}`)
            .then(res => {
                let data = res.data
                console.log(data)
                setInputName(data.name)
                setInputMK(data.course)
                setInputNilai(data.score)
                setCurrentId(data.id)
            })
    }


    const handleDelete = (event) => {
        let idStudent = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idStudent}`)
            .then(() => {
                let newPesertaLomba = penilaian.filter(el => { return el.id !== idStudent })
                setPenilaian(newPesertaLomba)
            })
    }

    const handleChangeName = (event) => {
        let inputValue = event.target.value
        setInputName(inputValue)
    }
    const handleChangeMK = (event) => {
        let inputValue = event.target.value
        setInputMK(inputValue)
    }
    const handleChangeNilai = (event) => {
        let inputValue = event.target.value
        setInputNilai(inputValue)
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        if (currentId === null) {
            // untuk create data baru
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputName, course: inputMK, score: inputNilai })
                .then(res => {
                    let data = res.data
                    setPenilaian([...penilaian, { id: data.id, name: data.name, course: data.course, score: data.score }])
                })
        } else {
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputName, course: inputMK, score: inputNilai })
                .then(() => {
                    let singlePeserta = penilaian.find(el => el.id === currentId)
                    singlePeserta.name = inputName
                    singlePeserta.course = inputMK
                    singlePeserta.score = inputNilai
                    setPenilaian([...penilaian])
                })
        }
        setInputName("")
        setInputMK("")
        setInputNilai("")
        setCurrentId(null)
    }

    return (
        <>
            {penilaian !== null &&
                (<div>
                    <h1>Daftar Nilai Mahasiswa</h1>
                    <table className="table-student">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Mata Kuliah</th>
                                <th>Nilai</th>
                                <th>Indeks Nilai</th>
                                <th>Akses</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                penilaian.map((item, index) => {
                                    console.log(item)
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.course}</td>
                                            <td>{item.score}</td>
                                            {item.score >= 80 && <td>A</td>}
                                            {item.score >= 70 && item.score < 80 && <td>B</td>}
                                            {item.score >= 60 && item.score < 70 && <td>C</td>}
                                            {item.score >= 50 && item.score < 60 && <td>D</td>}
                                            {item.score < 50 && <td>E</td>}
                                            <td>
                                                <button className="btn-yellow" onClick={handleEdit} value={item.id}>Edit</button>
                                                &nbsp;
                                                <button className="btn-red" onClick={handleDelete} value={item.id}>Delete</button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                    {/* Form */}
                    <h1>Form Nilai</h1>
                    <form className="form-student" style={{ paddingBottom: "20px" }} onSubmit={handleSubmit}>
                        <table>
                            <tr>
                                <td>Nama:</td>
                                <td>
                                    <input type="text" value={inputName} onChange={handleChangeName} required />
                                </td>
                            </tr>
                            <tr>
                                <td>Matakuliah:</td>
                                <td>
                                    <input type="text" value={inputMK} onChange={handleChangeMK} required />
                                </td>
                            </tr>
                            <tr>
                                <td>Nilai:</td>
                                <td>
                                    <input type="number" max="100" min="0" value={inputNilai} onChange={handleChangeNilai} required />
                                </td>
                            </tr>
                        </table>
                        <button>submit</button>
                    </form>
                </div>)
            }

        </>
    )
}

export default StudentScore