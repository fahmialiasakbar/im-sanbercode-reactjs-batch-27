var filterBooksPromise = require('./promise2.js')

function filterBook(isColorful, amountOfPage) {
    try {
        var result = filterBooksPromise(isColorful, amountOfPage)
        console.log(result)
    } catch (err) {
        console.log(err.message)
    }
}
async function filterBookAsync(isColorful, amountOfPage) {
    try {
        var result = await filterBooksPromise(isColorful, amountOfPage)
        console.log(result)
    } catch (err) {
        console.log(err.message)
    }
}

filterBook(true, 40)
filterBookAsync(false, 250)
filterBookAsync(30)