var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

let time = 10000
let indeks = 0
function execute(indeks) {
    readBooks(time, books[indeks], function (sisa) {
        if (sisa != 0) {
            indeks += 1
            time = sisa
            execute(indeks)
        }
    })
}

execute(indeks)