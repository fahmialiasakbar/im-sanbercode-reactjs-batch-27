var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let time = 10000
let indeks = 0
function execute(indeks) {
    readBooksPromise(time, books[indeks])
    .then(function (sisa) {
        if (sisa != 0) {
            indeks += 1
            time = sisa
            if(books[indeks]){
                execute(indeks)
            }
        }
    })
}

execute(indeks)