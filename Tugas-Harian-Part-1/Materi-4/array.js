
// PUSH
var numbers = [0,1,2]
numbers.push(3)
console.log(numbers)
numbers.push(4, 5)
console.log(numbers)

// POP
var numbers = [0, 1, 2, 3, 4, 5]
numbers.pop()
console.log(numbers)

// UNSHIFT
var numbers = [0, 1, 2, 3]
numbers.unshift(-1)
console.log(numbers)

// SHIFT
var numbers = [0, 1, 2, 3,]
numbers.shift()
console.log(numbers)

// SORT
var animals = ["kera", "gajah", "musang"]
animals.sort()
console.log(animals)

// SLICE
var angka = [0, 1, 2, 3]
var irisan1 = angka.slice(1, 3)
console.log(irisan1)
var irisan2 = angka.slice(0, 2)
console.log(irisan2)

// SPLICE
var fruits = ["banana", "orange", "grape"]
fruits.splice(1, 0, "watermelon")
console.log(fruits)
var fruits = [ "banana", "orange", "grape"]
fruits.splice(0, 2)
console.log(fruits)

// SPLIT
var kadalijo = "janji:palsu:pejabat"
var jo = kadalijo.split(":")
console.log(jo)

// JOIN
var children = ["jack", "gary", "mischa"]
var ch = children.join(", ")
console.log(ch)