console.log("While Loop")
var flag = 1

while(flag < 10){
    console.log("Interasi ke-" + flag)
    flag++
}

console.log("\nWhile Loop 2")
var deret = 5
var jumlah = 0
while(deret > 0){
    jumlah += deret
    deret--
    console.log("Jumlah saat ini: " + jumlah)
}
console.log(jumlah)

console.log("\nWhile Loop 3")
var i = 0
while(i < 5){
    if(i === 3){
        console.log("Ini while dengan kondisi")
    }else{
        console.log(i)
    }
    i++
}

console.log("\nFor Loop 1")
for(var angka = 1; angka < 10; angka++){
    console.log("Iterasi ke-" + angka)
}

console.log("\nFor Loop 2")
var jumlah = 0
for(var deret = 5; deret > 0; deret--){
    jumlah += deret
    console.log("Jumlah saat ini: " + jumlah)
}
console.log("Jumlah terakhir : " + jumlah)


console.log("\nFor Loop 3")
for(var deret = 0; deret < 10; deret += 2) {
    console.log('Iterasi dengan Increment counter 2: ' + deret);
}

console.log('-------------------------------');
   
for(var deret = 15; deret > 0; deret -= 3) {
    console.log('Iterasi dengan Decrement counter : ' + deret);
} 


console.log("\nFor Loop 4")
for(var i = 0; i <= 6 ; i++){
    if(i === 3){
        console.log("ini For-Loop dengan Kondisi")
    }else{
        console.log(i)
    }
}