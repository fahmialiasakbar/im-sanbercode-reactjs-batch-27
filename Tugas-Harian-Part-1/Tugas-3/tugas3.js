// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
console.log("...Jawaban Soal 1");
console.log(kataPertama.concat(" " + kataKedua.substr(0,1).toUpperCase() + kataKedua.substr(1)).concat(" " + kataKetiga.substr(0,6) + kataKetiga.substr(6,1).toUpperCase()).concat(" " + kataKeempat.toUpperCase()))

// soal 2
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang = 2 * (Number(panjangPersegiPanjang) + Number(lebarPersegiPanjang));
var luasSegitiga = 1 / 2 * Number(alasSegitiga) * Number(tinggiSegitiga);

console.log("...Jawaban Soal 2");
console.log("Keliling persegi panjang adalah",kelilingPersegiPanjang);
console.log("Luas segitiga adalah",luasSegitiga);

// soal 3
var sentences= 'wah javascript itu keren sekali'; 

var firstWord= sentences.substring(0, 3); 
var secondWord= sentences.substring(4, 15);
var thirdWord= sentences.substring(15, 19);
var fourthWord= sentences.substring(19, 25);
var fifthWord= sentences.substring(25, 31);

console.log("...Jawaban Soal 3");
console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);


// soal 4
var nilaiJohn = 80;
var nilaiDoe = 50;
var indeksJohn;
var indeksDoe;

if(nilaiJohn >= 80){
    indeksJohn = 'A';
}else if(nilaiJohn >= 70){
    indeksJohn = 'B';
}else if(nilaiJohn >= 60){
    indeksJohn = 'C';
}else if(nilaiJohn >= 50){
    indeksJohn = 'D';
}else{
    indeksJohn = 'E';
}

if(nilaiDoe >= 80){
    indeksDoe = 'A';
}else if(nilaiDoe >= 70){
    indeksDoe = 'B';
}else if(nilaiDoe >= 60){
    indeksDoe = 'C';
}else if(nilaiDoe >= 50){
    indeksDoe = 'D';
}else{
    indeksDoe = 'E';
}

console.log("...Jawaban Soal 4");
console.log("Indeks nilai John adalan " + indeksJohn);
console.log("Indeks nilai Doe adalan " + indeksDoe);

// soal 5
var tanggal = 21;
var bulan = 3;
var tahun = 2001;
var namaBulan;

switch(bulan){
    case 1 : { namaBulan = 'Januari'; break; }
    case 2 : { namaBulan = 'Februari'; break; }
    case 3 : { namaBulan = 'Maret'; break; }
    case 4 : { namaBulan = 'April'; break; }
    case 5 : { namaBulan = 'Mei'; break; }
    case 6 : { namaBulan = 'Juni'; break; }
    case 7 : { namaBulan = 'Juli'; break; }
    case 8 : { namaBulan = 'Agustus'; break; }
    case 9 : { namaBulan = 'September'; break; }
    case 10 : { namaBulan = 'Oktober'; break; }
    case 11 : { namaBulan = 'November'; break; }
    case 12 : { namaBulan = 'Desember'; break; }
}

console.log("...Jawaban Soal 5");
console.log("Saya lahir pada",tanggal,namaBulan,tahun);