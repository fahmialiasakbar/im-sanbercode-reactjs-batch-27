console.log("----- Soal 1 ------")
let r = 7;
const pi = 22 / 7;
const luasLingkaran = (r) => {
    return pi * r * r
}

const kelilingLingkaran = (r) => {
    return 2 * pi * r
}
console.log(luasLingkaran(r))
console.log(kelilingLingkaran(r))


console.log("----- Soal 2 ------")
let panggilan;
const introduce = (name, age, gender, profession) => {
    if (gender == 'Laki-Laki') {
        panggilan = 'Pak';
    } else {
        panggilan = 'Bu';
    }
    return `${panggilan} ${name} adalah seorang ${profession} yang berusia ${age} tahun`
}
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"



console.log("----- Soal 3 ------")
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
        }
    }
}

console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()


console.log("----- Soal 4 ------")
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 // kode diatas ini jangan di rubah atau di hapus sama sekali
const {name : phoneName, brand : phoneBrand, year, colors} = phone
const [colorBronze, colorWhite, colorBlack] = colors
 // kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 


console.log("----- Soal 5 ------")
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}

buku.warnaSampul = [ ...buku.warnaSampul, ...warna ]
newBuku = { ...buku, ...dataBukuTambahan}
console.log(newBuku)