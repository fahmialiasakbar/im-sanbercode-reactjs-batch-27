function tampilkan() {
    console.log("halo!");
}

tampilkan();



function munculkanAngkaDua() {
    return 2
}

var tampung = munculkanAngkaDua();
console.log(tampung)



function kalikanDua(angka) {
    return angka * 2
}

var tampung = kalikanDua(2);
console.log(tampung)


function tampilkanAngka(angkaPertama, angkaKedua) {
    return angkaPertama + angkaKedua
}

console.log(tampilkanAngka(5, 3))


function tampilkanAngka(angka = 1) {
    return angka
}

console.log(tampilkanAngka(5)) // 5, sesuai dengan nilai parameter yang dikirim
console.log(tampilkanAngka()) // 1, karena default dari parameter adalah 1



function tampilAngka(angkaPertama, angkaKedua) {

    var hasil = angkaPertama + angkaKedua

    if (hasil > 10) {
        return "hasil lebih besar dari 10"
    } else if (hasil > 0 && hasil < 10) {
        return "hasil lebih kecil dari 10"
    } else if (hasil === 0) {
        return "hasil 0"
    } else {
        return "Tidak ada nilai dari parameter"
    }
}



function looping(iteration) {
    for (var i = 0; i < iteration; i++) {
        console.log(i)
    }
}

looping(2)

var fungsiPerkalian = function (angkaPertama, angkaKedua) {
    return angkaPertama * angkaKedua
}
console.log(fungsiPerkalian(2, 4))