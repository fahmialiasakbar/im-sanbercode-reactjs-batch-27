var car = {
    brand: "Ferrari",
    type: "Sports Car",
    price: 50000000,
    "horse power": 986
}

var car2 = {}
// meng-assign key:value dari object car2
car2.brand = "Lamborghini"
car2.brand = "Sports Car"
car2.price = 100000000 
car2["horse power"] = 730 


var motorcycle1 = {    
    brand: "Handa",
    type: "CUB",
    price: 1000
}
console.log(motorcycle1.brand) // "Handa"
console.log(motorcycle1.type) // "CUB"




var mobil = [
    {merk: "BMW", warna: "merah", tipe: "sedan"}, 
    {merk: "toyota", warna: "hitam", tipe: "box"}, 
    {merk: "audi", warna: "biru", tipe: "sedan"}
]
console.log(mobil[0].merk) 



var mobil =[
    {
       "merk":"BMW",
       "warna":"merah",
       "tipe":"sedan"
    },
    {
       "merk":"toyota",
       "warna":"hitam",
       "tipe":"box"
    },
    {
       "merk":"audi",
       "warna":"biru",
       "tipe":"sedan"
    }
 ]

 mobil.forEach(function(item){
    console.log("warna : " + item.warna)
 })

 var arrayWarna = mobil.map(function(item){
    return item.warna
 })
 
 console.log(arrayWarna)

 var arrayMobilFilter = mobil.filter(function(item){
    return item.tipe != "sedan";
 })
 
 console.log(arrayMobilFilter)