// Soal 1
console.log("\n...Jawaban Soal 1")
console.log("LOOPING PERTAMA")
x = 2;
while(x <= 20){
    console.log(x + " - I love coding")
    x += 2
}

console.log("LOOPING KEDUA")
x = 20
while(x >= 2){
    console.log(x + " - I will become a frontend developer")
    x -= 2
}


// Soal 2
console.log("\n...Jawaban Soal 2")
var msg;
for(var i = 1; i <= 20; i++){
    if(i % 2 == 1){
        if(i % 3 == 0){
            msg = "I Love Coding"
        }else{
            msg = "Santai"
        }
    }else{
        msg = "Berkualitas"
    }
    console.log(i + " - " + msg)
}

// Soal 3
console.log("\n...Jawaban Soal 3")
var n = 7;
for(var i = 1; i <= n; i++){
    var output = ""
    for(var j = 1; j <=i; j++){
        output += "#"
    }
    console.log(output)
}

// Soal 4
var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
kalimat.shift()
kalimat.splice(1,1)
console.log("\n...Jawaban Soal 4")
console.log(kalimat.join(" "))


// Soal 5
var sayuran = []
sayuran.push("Kangkung")
sayuran.push("Bayam")
sayuran.push("Buncis")
sayuran.push("Kubis")
sayuran.push("Timun")
sayuran.push("Seledri")
sayuran.push("Tauge")
sayuran.sort()
console.log("\n...Jawaban Soal 5")
for(var i = 0; i < sayuran.length; i++){
    console.log(Number(i+1) + ". " + sayuran[i])
}